<?php

namespace Drupal\dashboard\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\dashboard\DashboardManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dashboard specific local action class.
 */
class DashboardLocalAction extends LocalActionDefault {

  /**
   * Constructs a BookVariationLocalAction object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider to load routes by name.
   * @param \Drupal\dashboard\DashboardManager $dashboardManager
   *   The dashboard manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteProviderInterface $route_provider,
    protected DashboardManager $dashboardManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.route_provider'),
      $container->get('dashboard.manager')
    );
  }

  public function getRouteParameters(RouteMatchInterface $route_match) {
    $parameters = parent::getRouteParameters($route_match);

    // Retrieve the default dashboard for the main dashboard route.
    if ($route_match->getRouteName() === 'dashboard') {
      $dashboard = $this->dashboardManager->getDefaultDashboard();
      $parameters['dashboard'] = $dashboard->id();
    }

    return $parameters;
  }

}
