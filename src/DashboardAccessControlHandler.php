<?php

namespace Drupal\dashboard;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the dashboard entity type.
 *
 * @see \Drupal\dashboard\Entity\Dashboard
 */
class DashboardAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * Constructs the dashboard access control handler instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dashboard\DashboardInterface $entity */
    if ($operation === 'view') {
      // Don't grant access to disabled dashboards.
      if (!$entity->status()) {
        return AccessResult::forbidden()->addCacheableDependency($entity);
      }

      // Use this approach to avoid user 1 to bypass permissions check in
      // AccountInterface::hasPermission().
      $role_storage = $this->entityTypeManager->getStorage('user_role');
      return AccessResult::allowedIf($role_storage->isPermissionInRoles("view {$entity->id()} dashboard", $account->getRoles()))
        ->addCacheContexts(['user.permissions']);
    }

    if ($operation === 'preview') {
      $permissions = [
        $this->entityType->getAdminPermission(),
        "view {$entity->id()} dashboard",
      ];
      return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
