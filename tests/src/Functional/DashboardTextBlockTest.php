<?php

namespace Drupal\Tests\Dashboard\Functional;

use Drupal\dashboard\Entity\Dashboard;
use Drupal\Tests\BrowserTestBase;

/**
 * Test for dashboard text block.
 *
 * @group dashboard
 */
class DashboardTextBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['dashboard', 'layout_builder'];

  /**
   * A user with permission to administer dashboards.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A role id with permissions to administer dashboards.
   *
   * @var string
   */
  protected $adminRole;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminRole = $this->drupalCreateRole([
      'access administration pages',
      'administer dashboard',
      'configure any layout',
    ]);

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->adminRole);
    $this->adminUser->save();

  }

  /**
   * Tests the block text addition.
   */
  public function testBlockText() {
    $dashboard = Dashboard::create([
      'id' => 'existing_dashboard',
      'label' => 'Existing Dashboard',
      'status' => TRUE,
    ]);
    $dashboard->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/structure/dashboard/existing_dashboard/layout');
    $this->assertSession()
      ->titleEquals('Edit layout for existing_dashboard | Drupal');
    $this->assertTrue(TRUE);

    // Add text block and save.
    $this->click('a[href="/layout_builder/choose/section/dashboard/existing_dashboard/0"]');
    $this->click('a[href="/layout_builder/configure/section/dashboard/existing_dashboard/0/layout_onecol"]');
    $this->click('form.layout-builder-configure-section input[type="submit"]');
    $this->click('a[href="/layout_builder/choose/block/dashboard/existing_dashboard/0/content"]');
    $this->click('a[href="/layout_builder/add/block/dashboard/existing_dashboard/0/content/dashboard_text_block"]');

    $this->submitForm([
      'settings[label]' => 'Block title',
      'settings[text][value]' => 'Block text',
    ], 'Add block');
    $this->click('form.dashboard-layout-builder-form input[type="submit"]');

    // Confirm that text block is added and config stored.
    $this->assertSession()->statusMessageContains(' Updated dashboard Existing Dashboard layout.', 'status');
    $this->click('a[href="/admin/structure/dashboard/existing_dashboard/layout"]');
    $this->assertSession()->pageTextContains('Block title');
    $this->assertSession()->pageTextContains('Block text');
  }

}
