<?php

namespace Drupal\Tests\dashboard\Functional;

use Drupal\dashboard\Entity\Dashboard;
use Drupal\Tests\BrowserTestBase;

/**
 * Test for dashboard redirects after login.
 *
 * @group dashboard
 */
class DashboardRedirectAfterLoginTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['dashboard'];

  /**
   * A user with permission to view dashboards.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A role id with permissions to view dashboards.
   *
   * @var string
   */
  protected $role;

  /**
   * A Dashboard to check access to.
   *
   * @var \Drupal\dashboard\DashboardInterface
   */
  protected $dashboard;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->dashboard = Dashboard::create([
      'id' => 'existing_dashboard',
      'status' => TRUE,
    ]);
    $this->dashboard->save();

    $this->role = $this->drupalCreateRole([
      'access administration pages',
      'view the administration theme',
      'view existing_dashboard dashboard',
    ]);

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->role);
    $this->adminUser->save();

    $this->drupalPlaceBlock('local_tasks_block');
  }

  public function testDashboardRedirectWhenThereIsADashboard() {
    $this->drupalLogin($this->adminUser);

    $this->assertSession()->addressEquals('/admin/dashboard');
  }

  public function testDashboardRedirectWhenThereIsNoDashboard() {
    $this->dashboard->delete();
    $this->drupalLogin($this->adminUser);

    $this->assertSession()->addressNotEquals('/admin/dashboard');
  }

  public function testDashboardRedirectWhenThereIsNoEnabledDashboard() {
    $this->dashboard->setStatus(FALSE)->save();
    $this->drupalLogin($this->adminUser);

    $this->assertSession()->addressNotEquals('/admin/dashboard');
  }

}
