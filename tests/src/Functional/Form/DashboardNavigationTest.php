<?php

namespace Drupal\Tests\dashboard\Functional;

use Drupal\dashboard\Entity\Dashboard;
use Drupal\Tests\BrowserTestBase;

/**
 * Test for dashboard navigation.
 *
 * @group dashboard
 */
class DashboardNavigationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['dashboard', 'toolbar'];

  /**
   * A user with permission to administer dashboards.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A role id with permissions to administer dashboards.
   *
   * @var string
   */
  protected $adminRole;

  /**
   * A role id with permissions to administer dashboards.
   *
   * @var string
   */
  protected $toolbarRole;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminRole = $this->drupalCreateRole([
      'access administration pages',
    ]);

    $this->toolbarRole = $this->drupalCreateRole([
      'view the administration theme',
      'access toolbar',
    ]);

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->adminRole);
    $this->adminUser->addRole($this->toolbarRole);
    $this->adminUser->save();
  }

  public function testDashboardToolbarItem() {
    $dashboard = Dashboard::create([
      'id' => 'existing_dashboard',
      'status' => TRUE,
    ]);
    $dashboard->save();

    $this->drupalLogin($this->adminUser);

    // Assert that the dashboard navigation item is present in the HTML.
    $this->assertSession()->elementExists('css', '#toolbar-administration #toolbar-link-dashboard');

    $this->adminUser->removeRole($this->adminRole);
    $this->adminUser->save();

    $this->drupalGet('<front>');
    // Assert that the dashboard navigation item is not present in the HTML.
    $this->assertSession()->elementNotExists('css', '#toolbar-administration #toolbar-link-dashboard');

  }

}
