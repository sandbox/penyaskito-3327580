<?php

namespace Drupal\Tests\dashboard\Functional;

use Drupal\dashboard\Entity\Dashboard;
use Drupal\Tests\BrowserTestBase;

/**
 * Test for dashboard form.
 *
 * @group dashboard
 */
class DashboardFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['dashboard'];

  /**
   * A user with permission to administer dashboards.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A role id with permissions to administer dashboards.
   *
   * @var string
   */
  protected $role;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->role = $this->drupalCreateRole([
      'access administration pages',
      'view the administration theme',
      'administer dashboard',
      'configure any layout',
    ]);

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->role);
    $this->adminUser->save();

    $this->drupalPlaceBlock('local_tasks_block', ['id' => 'primary_local_tasks']);
  }

  public function testDashboardAdd() {
    $this->drupalLogin($this->adminUser);

    // Create dashboard to edit.
    $edit = [];
    $edit['id'] = strtolower($this->randomMachineName(8));
    $edit['label'] = $this->randomString(8);
    $edit['description'] = $this->randomString(16);
    $edit['status'] = TRUE;

    $this->drupalGet('admin/structure/dashboard/add');
    $this->submitForm($edit, 'Save');

    // Check that the title and body fields are displayed with the correct values.
    $this->assertSession()->pageTextContains("Created new dashboard {$edit['label']}");
  }

  public function testDashboardFormLocalTasks() {
    $dashboard = Dashboard::create([
      'id' => 'existing_dashboard',
      'label' => 'Existing',
    ]);
    $dashboard->save();

    $this->drupalLogin($this->adminUser);

    // Check local actions in Edit form.
    $this->drupalGet('admin/structure/dashboard/existing_dashboard');

    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[1]/a', 'Edit(active tab)');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[2]/a', 'Edit Layout');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[3]/a', 'Preview');

    // Check local actions in Edit Layout form.
    $this->drupalGet('admin/structure/dashboard/existing_dashboard/layout');

    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[1]/a', 'Edit');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[2]/a', 'Edit Layout(active tab)');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[3]/a', 'Preview');

    // Check local actions in Preview tab.
    $this->drupalGet('admin/structure/dashboard/existing_dashboard/preview');

    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[1]/a', 'Edit');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[2]/a', 'Edit Layout');
    $this->assertSession()->elementTextEquals('xpath', '//*[@id="block-primary-local-tasks"]/ul/li[3]/a', 'Preview(active tab)');
  }

}
