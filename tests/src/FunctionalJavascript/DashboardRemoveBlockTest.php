<?php

namespace Drupal\Tests\dashboard\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the JS functionality in the block add form.
 *
 * @group block
 */
class DashboardRemoveBlockTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dashboard_test',
    'dashboard',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer dashboards.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A role id with permissions to administer dashboards.
   *
   * @var string
   */
  protected $role;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->role = $this->drupalCreateRole([
      'access administration pages',
      'view the administration theme',
      'administer dashboard',
      'configure any layout',
      'view test dashboard',
    ]);

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->role);
    $this->adminUser->save();

    $this->drupalPlaceBlock('local_tasks_block', ['id' => 'primary_local_tasks']);
  }

  public function testDashboardRemoveBlock() {
    // Login with adequate permissions.
    $this->drupalLogin($this->adminUser);

    // Check that test dashboard exists.
    $this->drupalGet('/admin/dashboard');
    $this->assertSession()->elementExists('css', '.dashboard--test');

    // Validate that 2 blocks are visible and no unsaved changes.
    $this->drupalGet('/admin/structure/dashboard/test/layout');
    $this->assertSession()->pageTextNotContains("You have unsaved changes.");
    $this->assertSession()->pageTextContains("Clear cache 1");
    $this->assertSession()->pageTextContains("Clear cache 2");

    // Remove one of the blocks
    $this->drupalGet('/layout_builder/remove/block/dashboard/test/0/second/3be7e244-44ba-4268-a189-bdc0fca52d92');
    $this->submitForm([], 'Remove');

    // Validate that one block is removed and there are unsaved changes.
    $this->drupalGet('/admin/structure/dashboard/test/layout');
    $this->assertSession()->pageTextContains("You have unsaved changes.");
    $this->assertSession()->pageTextContains("Clear cache 1");
    $this->assertSession()->pageTextNotContains('Clear cache 2');

    // Save changes.
    $this->submitForm([], 'Save dashboard layout');

    // Validate that one block is removed and there are no unsaved changes.
    $this->drupalGet('/admin/structure/dashboard/test/layout');
    $this->assertSession()->pageTextNotContains("You have unsaved changes.");
    $this->assertSession()->pageTextContains("Clear cache 1");
    $this->assertSession()->pageTextNotContains('Clear cache 2');
  }

}
